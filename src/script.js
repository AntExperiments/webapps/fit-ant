const example = {
    data() {
        return {
            isActive: false
        }
    }
}

async function makeRandom() {
    await new Promise(resolve => setTimeout(resolve, 10));
    document.getElementsByClassName("media-content")[0].innerHTML = getTask();
}

function getTask() {
    let task = Math.floor((Math.random() * 4) + 1);
    minAmmount = 2;
    maxAmmount = document.getElementsByClassName("b-slider-fill")[0].style.width.replace('%', '') / 2;
    console.log(minAmmount)
    console.log(maxAmmount)
    let ammount = Math.floor((Math.random() * maxAmmount) + minAmmount);

    switch (task) {
        case 1:
            return "Do " + ammount * 2 + " pushups";
        case 2:
            return "Do " + ammount * 3 + " sittups";
        case 3:
            return "Do " + ammount * 1 + " jumps";
        case 4:
            return "Do " + ammount * 1 + " squats";
        default:
            return "Do nothing, an error happened";
    }
}

const app = new Vue(example)
app.$mount('#app')

// service worker

if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service-worker.js', { scope: '/' })
      .then(function(registration) {
            console.log('Service Worker Registered');
      });

    navigator.serviceWorker.ready.then(function(registration) {
       console.log('Service Worker Ready');
    });
  }